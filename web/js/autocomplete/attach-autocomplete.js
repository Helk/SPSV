(function( $ ) {

	var proto = $.ui.autocomplete.prototype,
	initSource = proto._initSource;

	function filter( array, term ) {
		var matcher = new RegExp( $.ui.autocomplete.escapeRegex(term), "i" );
		return $.grep( array, function(value) {
			return matcher.test( $( "<div>" ).html( value.label || value.value || value ).text() );
		});
	}

	$.extend( proto, {
		_initSource: function() {
			if ( this.options.html && $.isArray(this.options.source) ) {
				this.source = function( request, response ) {
					response( filter( this.options.source, request.term ) );
				};
			} else {
				initSource.call( this );
			}
		},

		_renderItem: function( ul, item) {
			return $( "<li></li>" )
			.data( "item.autocomplete", item )
			.append( $( "<a></a>" )[ this.options.html ? "html" : "text" ]( item.label ) )
			.appendTo(ul);
		}
	});
})( jQuery );

var lastXhr, suggestions, selected_id = null;
jQuery(document).ready(function($){
	$("#searchbox")
		.autocomplete({
			minLength: 1,
			autoFocus: true,
			html: true,
			source: function( request, response ) {

				$.ajax({
					url: TWIG.path,
					dataType: "json",
					data : request,
					type: 'POST',
					success: function (data)
					{
						for(d in data){
							var city_info = data[d].label.split('|');
							data[d].label = '';
							data[d].label += '<div class="city-text">';
							data[d].label += '<div class="city-name">';
							data[d].label += city_info[0];
							data[d].label += '</div>';
							data[d].label += '<div class="postal-code">';
							data[d].label += city_info[1];
							data[d].label += '</div>';
							data[d].label += '</div>';
						}
						suggestions = data;
						response( data );
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						console.log(textStatus, errorThrown);
					}
				});
			},
			select: function(event, ui) {
				selected_id = ui.item.id;
				$('#selected-city').val(selected_id);
				var city_name = $(ui.item.label).find('.city-name').text();
				$('#city-name').val(city_name);
			},
			change: function(event, ui){
				if (selected_id === null){
					var found = false;
					for(i in suggestions){
						if (suggestions[i].value.toLowerCase() == $(this).val().toLowerCase()){
							selected_id = suggestions[i].id;
							$('#selected-city').val(selected_id);
							$(this).val(suggestions[i].value);
							found = true;
						}
					}
					if (!found){
					}
				}
			},
			open: function(){
				selected_id = null;
				$('#selected-city').val(selected_id);
			},
			appendTo: '#city-list'
		})
	;
});
