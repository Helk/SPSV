<?php

namespace SPV\GeolocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MapsVille
 *
 * @ORM\Table(name="maps_ville")
 * @ORM\Entity(repositoryClass="SPV\GeolocationBundle\Repository\MapsVilleRepository")
 */
class MapsVille
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_ville", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="cp", type="string", length=5)
     */
    private $cp;

    /**
     * @var decimal
     *
     * @ORM\Column(name="lat", type="decimal", precision=7, scale=5, nullable=true)
     */
    private $lat;

    /**
     * @var decimal
     *
     * @ORM\Column(name="lon", type="decimal", precision=7, scale=5, nullable=true)
     */
    private $lon;

    /**
     * @ORM\ManyToOne(targetEntity="SPV\GeolocationBundle\Entity\Departement", cascade={"persist"})
     * @ORM\JoinColumn(name="id_departement", referencedColumnName="id_departement", nullable=false)
     */
     private $departement;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return MapsVille
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set cp
     *
     * @param string $cp
     *
     * @return MapsVille
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set lat
     *
     * @param float $lat
     *
     * @return MapsVille
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param float $lon
     *
     * @return MapsVille
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return float
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set departement
     *
     * @param \SPN\CountryBundle\Entity\Departement $departement
     *
     * @return MapsVille
     */
    public function setDepartement(\SPV\GeolocationBundle\Entity\Departement $departement)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return \SPN\CountryBundle\Entity\Departement
     */
    public function getDepartement()
    {
        return $this->departement;
    }
}
