<?php
// src/AppBundle/Form/RegistrationType.php

namespace SPV\GeolocationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use SPV\PartyBundle\Form\FormType\PlaceType;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('address', TextType::class, array('label' => 'Adresse'))
                ->add('streetNumber', IntegerType::class, array('label' => 'Numéro de la rue',
                                                                'attr' => array('readonly' => true)))
                ->add('route', TextType::class, array('label' => 'Rue',
                                                      'attr' => array('readonly' => true)))
                ->add('locality', TextType::class, array('label' => 'Ville',
                                                         'attr' => array('readonly' => true)))
                ->add('administrativeAreaLevel1', TextType::class, array('label' => 'Région',
                                                                         'attr' => array('readonly' => true)))
                ->add('administrativeAreaLevel2', TextType::class, array('label' => 'Département',
                                                                          'attr' => array('readonly' => true)))
                ->add('country', TextType::class, array('label' => 'Pays', 'attr' => array('readonly' => true)))
                ->add('lat', NumberType::class, array('attr' => array('readonly' => true)))
                ->add('lng', NumberType::class, array('attr' => array('readonly' => true)))
                ->add('placeId', TextType::class, array('attr' => array('readonly' => true)))
                ->add('postalCode', TextType::class, array('label' => 'Code postal', 'attr' => array('readonly' => true)))
                ;
    }
}
