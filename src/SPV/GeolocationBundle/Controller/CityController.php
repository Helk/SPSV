<?php

namespace SPV\GeolocationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CityController extends Controller
{
    public function ajaxSearchAction(Request $request)
    {
        if($request->isXmlHttpRequest())
        {
            $term = $request->request->get('term');
            $array= $this->getDoctrine()
                ->getManager()
                ->getRepository('GeolocationBundle:MapsVille')
                ->searchCitiesByNameOrPostalCode($term);

            $response = new Response(json_encode($array));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
    }
}
