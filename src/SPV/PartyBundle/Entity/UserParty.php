<?php

namespace SPV\PartyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="SPV\PartyBundle\Repository\UserPartyRepository")
 */
class UserParty
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="SPV\UserBundle\Entity\User", cascade={"persist"})
     */
    private $user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="SPV\PartyBundle\Entity\Party", cascade={"persist"}, inversedBy="participants")
     */
    private $party;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isParticipating", type="boolean")
     */
    private $isParticipating;

    /**
     * @var boolean
     *
     * @ORM\Column(name="notParticipatingAnymore", type="boolean")
     */
    private $notParticipatingAnymore;

    public function __construct()
    {
        $this->isParticipating = false;
        $this->notParticipatingAnymore = false;
    }

    /**
     * Set isParticipating
     *
     * @param boolean $isParticipating
     *
     * @return UserParty
     */
    public function setIsParticipating($isParticipating)
    {
        $this->isParticipating = $isParticipating;

        return $this;
    }

    /**
     * Get isParticipating
     *
     * @return boolean
     */
    public function getIsParticipating()
    {
        return $this->isParticipating;
    }

    /**
     * Set notParticipatingAnymore
     *
     * @param boolean $notParticipatingAnymore
     *
     * @return UserParty
     */
    public function setNotParticipatingAnymore($notParticipatingAnymore)
    {
        $this->notParticipatingAnymore = $notParticipatingAnymore;

        return $this;
    }

    /**
     * Get notParticipatingAnymore
     *
     * @return boolean
     */
    public function getNotParticipatingAnymore()
    {
        return $this->notParticipatingAnymore;
    }

    /**
     * Set user
     *
     * @param \SPV\UserBundle\Entity\User $user
     *
     * @return UserParty
     */
    public function setUser(\SPV\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \SPV\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set party
     *
     * @param \SPV\PartyBundle\Entity\Party $party
     *
     * @return UserParty
     */
    public function setParty(\SPV\PartyBundle\Entity\Party $party)
    {
        $this->party = $party;

        return $this;
    }

    /**
     * Get party
     *
     * @return \SPV\PartyBundle\Entity\Party
     */
    public function getParty()
    {
        return $this->party;
    }
}
