<?php

namespace SPV\PartyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Party
 *
 * @ORM\Table(name="party")
 * @ORM\Entity(repositoryClass="SPV\PartyBundle\Repository\PartyRepository")
 */
class Party
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="place_type", type="integer")
     * @Assert\NotBlank()
     */
    private $placeType;

    /**
     * @ORM\ManyToOne(targetEntity="SPV\GeolocationBundle\Entity\Address", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     * @Assert\NotBlank()
     */
    private $datetime;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="SPV\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isCanceled", type="boolean")
     */
    private $isCanceled;

    /**
     * @var int
     *
     * @ORM\Column(name="participant_number", type="integer", nullable=true)
     */
    private $participantNumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isPro", type="boolean")
     */
    private $isPro;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=30)
     * @Assert\NotBlank()
     * @Assert\Length(
     *    min = 4,
     *    max = 30,
     *    minMessage="La taille du titre doit faire au minimum {{ limit }} caractères.",
     *    maxMessage="La taile du titre doit faire au maximum {{ limit }} caractères."
     * )
     */
     private $title;

    /**
     * @ORM\OneToMany(targetEntity="SPV\PartyBundle\Entity\UserParty", mappedBy="party")
     */
    private $participants;

    public function __construct()
    {
        $this->isCanceled  = false;
        $this->isPro = false;
        $this->createdAt = new \DateTime;
        $this->datetime = new \DateTime;
        $this->participants = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set placeType
     *
     * @param string $placeType
     *
     * @return Party
     */
    public function setPlaceType($placeType)
    {
        $this->placeType = $placeType;

        return $this;
    }

    /**
     * Get placeType
     *
     * @return string
     */
    public function getPlaceType()
    {
        return $this->placeType;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Party
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Party
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Party
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set isCanceled
     *
     * @param boolean $isCanceled
     *
     * @return Party
     */
    public function setIsCanceled($isCanceled)
    {
        $this->isCanceled = $isCanceled;

        return $this;
    }

    /**
     * Get isCanceled
     *
     * @return boolean
     */
    public function getIsCanceled()
    {
        return $this->isCanceled;
    }

    /**
     * Set participantNumber
     *
     * @param integer $participantNumber
     *
     * @return Party
     */
    public function setParticipantNumber($participantNumber)
    {
        $this->participantNumber = $participantNumber;

        return $this;
    }

    /**
     * Get participantNumber
     *
     * @return integer
     */
    public function getParticipantNumber()
    {
        return $this->participantNumber;
    }

    /**
     * Set address
     *
     * @param \SPV\GeolocationBundle\Entity\Address $address
     *
     * @return Party
     */
    public function setAddress(\SPV\GeolocationBundle\Entity\Address $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \SPV\GeolocationBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set createdBy
     *
     * @param \SPV\UserBundle\Entity\User $createdBy
     *
     * @return Party
     */
    public function setCreatedBy(\SPV\UserBundle\Entity\User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \SPV\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function formatedPlace()
    {
        switch ($this->placeType) {
          case 0:
            return "Restaurant";
            break;

          case 1:
            return "Bar";
            break;

          case 2:
            return "Salon de thé";
            break;

          case 3:
            return "Discothèque";
            break;

          case 4:
            return "A la maison";
            break;

          default:
            return "Autre lieu";
            break;
        }
    }

    /**
     * Add participant
     *
     * @param \SPV\PartyBundle\Entity\UserParty $participant
     *
     * @return Party
     */
    public function addParticipant(\SPV\PartyBundle\Entity\UserParty $participant)
    {
        $this->participants[] = $participant;

        return $this;
    }

    /**
     * Remove participant
     *
     * @param \SPV\PartyBundle\Entity\UserParty $participant
     */
    public function removeParticipant(\SPV\PartyBundle\Entity\UserParty $participant)
    {
        $this->participants->removeElement($participant);
    }

    /**
     * Get participants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Party
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set isPro
     *
     * @param boolean $isPro
     *
     * @return Party
     */
    public function setIsPro($isPro)
    {
        $this->isPro = $isPro;

        return $this;
    }

    /**
     * Get isPro
     *
     * @return boolean
     */
    public function getIsPro()
    {
        return $this->isPro;
    }
}
