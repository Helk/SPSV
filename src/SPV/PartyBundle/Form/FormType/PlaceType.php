<?php

namespace SPV\PartyBundle\Form\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PlaceType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array(
               'Restaurant' => 0,
               'Bar' => 1,
               'Salon de thé' => 2,
               'Discothèque' => 3,
               'A la maison' => 4,
               'Autre' => 5
            )
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
