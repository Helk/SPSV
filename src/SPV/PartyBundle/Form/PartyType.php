<?php
// src/AppBundle/Form/RegistrationType.php

namespace SPV\PartyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use SPV\PartyBundle\Form\FormType\PlaceType;
use SPV\GeolocationBundle\Form\AddressType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class PartyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('placeType', PlaceType::class, array('label' => 'Lieu',
                                                            'multiple' => false,
                                                            'expanded' => false))
                ->add('datetime', DateTimeType::class, array('label' => 'Date et heure', 'widget' => 'single_text'))
                ->add('address', AddressType::class, array("data_class" => 'SPV\GeolocationBundle\Entity\Address'))
                ->add('participantNumber', IntegerType::class, array('label' => 'Nombre de participant maximal', 'required' => false))
                ->add('description', TextareaType::class, array('label' => 'Description de la soirée',
                                                                'attr' => array('rows' => 5)))
                ->add('title', TextType::class, array('label' => "Titre de la soirée"))
                ;
    }
}
