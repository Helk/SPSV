<?php

namespace SPV\PartyBundle\Controller;

use SPV\PartyBundle\Form\PartyType;
use SPV\PartyBundle\Entity\Party;
use SPV\PartyBundle\Entity\UserParty;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $connectedUser = $this->getUser();

        if($connectedUser != null)
        {
            $em = $this->getDoctrine()->getManager();
            $parties = $em->getRepository('PartyBundle:Party')
                          ->findBy(array('createdBy' => $connectedUser));

            return $this->render('PartyBundle::index.html.twig', array('parties' => $parties));
        }

        return $this->render('PartyBundle::index.html.twig');
    }

    public function organizeAction(Request $request)
    {
        return $this->save($this->getUser(), null, $request);

    }

    public function modifyAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $party = $em->getRepository("PartyBundle:Party")->find($id);

        return $this->save($this->getUser(), $party, $request);
    }

    private function save($connectedUser, $party, Request $request)
    {
        $isChanging = true;

        if($party == null)
        {
            $party = new Party();
            $isChanging = false;
        }

        $form = $this->createForm(PartyType::class, $party);

        if($connectedUser != null)
        {
            if($request->getMethod() == 'POST')
            {
                //On fait le lien entre la requête et le formulaire Request <=> Form
                $form->handleRequest($request);

                if($form->isValid())
                {
                    $em = $this->getDoctrine()->getManager();
                    //Ici on créé le message à l'heure actuel on le fait pour un utilisateur
                    //Pour plusieurs il faudrait ajouter un foreach
                    $party->setCreatedBy($connectedUser);

                    if(!$isChanging)
                    {
                      $em->persist($party);
                    }
                    $em->flush();

                    if (!$isChanging) {
                      $this->get('session')->getFlashBag()->add('success', "La soirée a été créée ");
                    }
                    else {
                      $this->get('session')->getFlashBag()->add('success', "La soirée a été modifié ");
                    }

                    //Redirection vers la page d'accueil
                    return $this->redirect($this->generateUrl('party_homepage'));
                }
            }
        }

        $array_view = array('form' => $form->createView());

        if($isChanging)
        {
            $array_view["partyId"] = $party->getId();
        }

        return $this->render('PartyBundle::organize.html.twig', $array_view);
    }

    public function showAction($id)
    {
        $connectedUser = $this->getUser();

        if($connectedUser != null)
        {
            $em = $this->getDoctrine()->getManager();
            $party = $em->getRepository("PartyBundle:Party")->find($id);

            if($party != null)
            {
                //Permet de voir si la personnes et la créatrice de la soirée
                $isCreator = false;

                if($party->getCreatedBy()->getId() === $connectedUser->getId())
                {
                    $isCreator = true;
                }

                $array_view = array("party" => $party, "isCreator" => $isCreator);

                $participant = $em->getRepository("PartyBundle:UserParty")
                                  ->findBy(array("party" => $party, "user" => $connectedUser));

                if($participant != null)
                {
                    $array_view["participant"] = $participant[0];
                }

                return $this->render('PartyBundle::show.html.twig', $array_view);
            }
        }

        return $this->render('PartyBundle::show.html.twig',
                              array("error" => "Cette soirée n'existe pas !"));
    }

    public function participateAction($id)
    {
        return $this->fromUser($id, $this->getUser(), false);
    }

    public function cancelParticipationAction($id)
    {
        return $this->fromUser($id, $this->getUser(), true);
    }

    //Supprime ou crée la participation de l'utilisateur
    private function fromUser($id, $connectedUser, $removePart)
    {
        $connectedUser = $this->getUser();

        if($connectedUser != null)
        {
            $em = $this->getDoctrine()->getManager();
            $party = $em->getRepository("PartyBundle:Party")->find($id);

            if($party != null)
            {
                if($removePart)
                {
                    $participant = $em->getRepository("PartyBundle:UserParty")
                                    ->findBy(array("party" => $party, "user" => $connectedUser))[0];
                    $em->remove($participant);
                }
                else {
                    $participant = new UserParty();
                    $participant->setParty($party);
                    $participant->setUser($connectedUser);
                    $participant->setIsParticipating(true);

                    $em->persist($participant);
                }

                $em->flush();

                return $this->redirect($this->generateUrl('party_show', array("id" => $id)));
            }
        }

        return $this->render('PartyBundle::show.html.twig',
                              array("error" => "Cette soirée n'existe pas !"));
    }

    public function cancelPartyAction($id)
    {
        $connectedUser = $this->getUser();

        if($connectedUser != null)
        {
            $em = $this->getDoctrine()->getManager();
            $party = $em->getRepository("PartyBundle:Party")->find($id);

              if($party != null)
              {
                  $party->setIsCanceled(true);

                  $emails = array();

                  foreach ($party->getParticipants() as $up) {
                    $emails[$up->getUser()->getEmail()] = $up->getUser()->getUsername();
                  }

                  $em->flush();
                  $this->sendCancellation($emails, $party);

                  return $this->redirect($this->generateUrl('party_show', array("id" => $id)));
              }
          }

          return $this->render('PartyBundle::show.html.twig',
                                array("error" => "Cette soirée n'existe pas !"));
    }

    private function sendCancellation($emails, $party)
    {
        //Envoi du mail d'activation
        $this->sendMailsBcc('Annulation de la soirée de ' . $party->getCreatedBy()->getUsername(),
                        'Email/Party/cancellation.html.twig',
                        $emails,
                        array('name' => $party->getCreatedBy()->getUsername(),
                              'address' => $party->getAddress()->getAddress(),
                              'datetime' => $party->getDatetime(),
                              'link' => $this->generateUrl('party_show', array("id" => $party->getId()), UrlGeneratorInterface::ABSOLUTE_URL)));
    }

    private function sendMailsBcc($subject, $view, $emails, $params)
    {
        //Récupération du service de mail
        $mailer = $this->get('mailer');

        $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom('nosupply@seulpourlasaintvalentin.com')
                    ->setBcc($emails)
                    ->setBody($this->renderView($view, $params), 'text/html');

        //Envoie de l'email
        $mailer->send($message);
    }
}
