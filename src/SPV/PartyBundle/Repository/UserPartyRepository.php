<?php

namespace SPV\PartyBundle\Repository;

class UserPartyRepository extends \Doctrine\ORM\EntityRepository
{
    public function getAllParticipation($idUser)
    {
        $qb = $this->createQueryBuilder('up');
        $qb ->select('up')
        ->where('up.user = :id')
        ->setParameter('id', $idUser)
        ->addOrderBy('up.isAccepted', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getAllParticipants($idParty)
    {
        $qb = $this->createQueryBuilder('up');
        $qb ->select('up')
        ->where('up.isValided = 1')
        ->andWhere('up.isAccepted = 1')
        ->andWhere('up.party = :id')
        ->setParameter('id', $idParty);

        return $qb->getQuery()->getResult();
    }

    public function getAllRequest($idParty)
    {
        $qb = $this->createQueryBuilder('up');
        $qb ->select('up')
        ->where('up.isAccepted = 0')
        ->andWhere('up.party = :id')
        ->addOrderBy('up.isValided', 'ASC')
        ->setParameter('id', $idParty);

        return $qb->getQuery()->getResult();
    }

    public function getNumberOfParticipant($idParty)
    {
        $qb = $this->createQueryBuilder('up');
        $qb ->select('count(up.user)')
        ->where('up.isValided = 1')
        ->andWhere('up.isAccepted = 1')
        ->andWhere('up.party = :id')
        ->setParameter('id', $idParty);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getNumberOfRequest($idParty)
    {
        $qb = $this->createQueryBuilder('up');
        $qb ->select('count(up.user)')
        ->where('up.isValided = 0')
        ->andWhere('up.party = :id')
        ->setParameter('id', $idParty);

        return $qb->getQuery()->getSingleScalarResult();
    }
}
