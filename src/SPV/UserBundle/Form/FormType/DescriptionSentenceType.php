<?php

namespace SPV\UserBundle\Form\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DescriptionSentenceType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array(
                'Je désire fêter mon célibat comme il se doit !' => '1',
                'Je désire célébrer mon célibat sobrement.' => '2',
                'Je désire être surpris(e) ...' => '3',
            )
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
