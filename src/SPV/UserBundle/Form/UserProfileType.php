<?php
// src/AppBundle/Form/RegistrationType.php

namespace SPV\UserBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use SPV\UserBundle\Form\FormType\DescriptionSentenceType;
use SPV\UserBundle\Form\FormType\GenderType;

class UserProfileType extends AbstractType
{
      public function buildForm(FormBuilderInterface $builder, array $options)
      {
          $builder->add('age', IntegerType::class, array('label' => 'Indiquez votre âge'))
                  ->add('gender', GenderType::class, array('label' => 'Indiquez votre sexe',
                                                           'expanded' => true,
                                                           'multiple' => false))
                  ->add('description', TextareaType::class, array('label' => 'Description de votre profil',
                                                                          'attr' => array('rows' => 3),
                                                                          'required' => false))
                  ->add('oneshotDescription', DescriptionSentenceType::class, array('label' => 'Choisissez la phrase qui vous correspond le mieux :',
                                                                                    'expanded' => true,
                                                                                    'multiple' => false))
                  ->add('file', FileType::class, array('required' => false, 'label' => 'Photo (format: jpeg, png, gif et tiff)'))
          ;
      }

      public function configureOptions(OptionsResolver $resolver)
      {
          $resolver->setDefaults(array(

          ));
      }

      public function getBlockPrefix()
      {
          return 'user_profile_registration';
      }
}
