<?php
// src/AppBundle/Form/RegistrationType.php

namespace SPV\UserBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ProfileType extends AbstractType
{
      private $class;

      /**
       * @param string $class The User class name
       */
      public function __construct($class)
      {
          $this->class = $class;
      }

      public function buildForm(FormBuilderInterface $builder, array $options)
      {
          $builder
              ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
              ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle'))
              ->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
                  'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
                  'options' => array('translation_domain' => 'FOSUserBundle'),
                  'first_options' => array('label' => 'form.new_password'),
                  'second_options' => array('label' => 'form.new_password_confirmation'),
                  'invalid_message' => 'fos_user.password.mismatch',
                  'required' => false
              ))
              ->add('current_password', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'), array(
                  'label' => 'form.current_password',
                  'translation_domain' => 'FOSUserBundle',
                  'mapped' => false,
                  'constraints' => new UserPassword(),
              ))
          ;
      }

      public function configureOptions(OptionsResolver $resolver)
      {
          $resolver->setDefaults(array(
              'data_class' => $this->class,
              'csrf_token_id' => 'profile',
              // BC for SF < 2.8
              'intention'  => 'profile',
          ));
      }

      public function getBlockPrefix()
      {
          return 'fos_user_profile';
      }
}
