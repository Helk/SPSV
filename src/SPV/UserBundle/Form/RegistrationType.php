<?php
// src/AppBundle/Form/RegistrationType.php

namespace SPV\UserBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class RegistrationType extends AbstractType
{
      private $class;

      /**
       * @param string $class The User class name
       */
      public function __construct($class)
      {
          $this->class = $class;
      }

      public function buildForm(FormBuilderInterface $builder, array $options)
      {
          $builder
              ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
              ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle'))
              ->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'), array(
                  'label' => 'form.password',
                  'translation_domain' => 'FOSUserBundle'
              ))
              ->add('wantTips', CheckboxType::class, array('label' => 'Recevoir les bons plans et astuces de Seul Pour la Saint-Valentin',
                                                           'required' => false
                                                         ))
              ->add('hasAcceptedCGU', CheckboxType::class, array('label' => "J'ai lu et j'accepte les conditions générales d'utilisation",
                                                                 'required' => true
                                                               ))
          ;
      }

      public function configureOptions(OptionsResolver $resolver)
      {
          $resolver->setDefaults(array(
              'data_class' => $this->class,
              'csrf_token_id' => 'registration',
              // BC for SF < 2.8
              'intention'  => 'registration',
          ));
      }

      public function getBlockPrefix()
      {
          return 'fos_user_registration';
      }
}
