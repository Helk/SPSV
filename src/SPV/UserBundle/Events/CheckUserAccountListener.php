<?php

namespace SPV\UserBundle\Events;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CheckUserAccountListener
{
    private $router;
    private $token;
    private $auth;

    public function __construct(UrlGeneratorInterface $router, TokenStorage $tokenStorage, AuthorizationCheckerInterface $auth)
    {
        $this->router = $router;
        $this->auth = $auth;
        $this->token = $tokenStorage;
    }

    private function getUser()
    {
        $token = $this->token->getToken();

        if($token != null){
          return $this->token->getToken()->getUser();
        }
        else {
          return null;
        }
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $user = $this->getUser();

        ///On redirige l'utilisateur vers la complétion du profile
        // si seulement si l'utlisateur n'est pas null et qu'il n'est pas annonyme
        if($user != null && $this->auth->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            //Si l'utilisateur n'a pas complété son profil
            //et qu'on est pas dans le controller de completion de profil
            //et qu'on ne fait pas de l'ajaxSearchAction
            //alors on le redirige vers la complétion du profil
            if ($user->getProfileIsCompleted() == false &&
                $event->getRequest()->attributes->get('_controller') != 'SPV\UserBundle\Controller\UserProfileController::completeAction' &&
                $event->getRequest()->attributes->get('_controller') != 'SPV\GeolocationBundle\Controller\CityController::ajaxSearchAction')
            {
                $url = $this->router->generate('user_complete_profile');
                $event->setResponse(new RedirectResponse($url));
            }
        }
    }
}
