<?php

namespace SPV\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('UserBundle:Default:index.html.twig');
    }

    public function removeAction()
    {
        $connectedUser = $this->getUser();

        if($connectedUser != null)
        {
            $em = $this->getDoctrine()->getManager();

            $ups = $em->getRepository('PartyBundle:UserParty')->findBy(array('user' => $connectedUser));
            $parties = $em->getRepository('PartyBundle:Party')->findBy(array('createdBy' => $connectedUser));

            foreach ($parties as $party) {
                $participation = $em->getRepository('PartyBundle:UserParty')->findBy(array('party' => $party));
                foreach ($participation as $participant) {
                    $em->remove($participant);
                }

                $em->remove($party);
            }

            foreach ($ups as $up) {
                $em->remove($up);
            }

            $umsgCreated = $em->getRepository('MessageBundle:UserMessage')->getCreatedMessages($connectedUser->getId());
            $ums = $em->getRepository('MessageBundle:UserMessage')->findBy(array('user' => $connectedUser->getId()));

            foreach ($umsgCreated as $pm) {
                $em->remove($pm);
            }

            foreach ($ums as $pm) {
                $em->remove($pm);
            }

            $em->remove($connectedUser);

            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('success', "Le compte a été supprimé ");

        //Redirection vers la page d'accueil
        return $this->redirect($this->generateUrl('homepage'));
    }
}
