<?php

namespace SPV\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use SPV\UserBundle\Form\UserProfileType;
use SPV\UserBundle\Entity\UserProfile;

class UserProfileController extends Controller
{
    public function completeAction(Request $request)
    {
        $connectedUser = $this->getUser();
        $userProfile = new UserProfile();

        $city_term = null;

        if($connectedUser != null)
        {
            //Si le profil est déjà complété alors c'est une modification
            //On récupère le user profile existant et la ville existante
            if($connectedUser->getProfileIsCompleted() != false)
            {
                $userProfile = $connectedUser->getUserProfile();
                //On met le nom de la ville qu'on avait
                $city_term = $userProfile->getCity()->getNom();
            }
        }

        $form = $this->createForm(UserProfileType::class, $userProfile);

        //Toutes le villes trouvées.
        //Ce tableau sera afffiché sous forme de liste
        //Si on trouve plusieurs villes ou codes postals
        $found_cities = array();

        //Erreurs liées à la ville
        $error_cities = array();

        if($connectedUser != null)
        {
            if($request->getMethod() == 'POST')
            {
                //Récupération du nom de la ville
                $city_term =  $request->request->get('city_term');
                $city_id = $request->request->get('cities');

                $isCityTermOk = $city_term != "" || $city_term != null;

                if($isCityTermOk != true && $city_id == "")
                {
                    array_push($error_cities, "Entrez une ville s'il vous plait !");
                }

                if($isCityTermOk || $city_id != "")
                {
                    //On fait le lien entre la requête et le formulaire Request <=> Form
                    $form->handleRequest($request);

                    $em = $this->getDoctrine()->getManager();
                    $repo_city = $em->getRepository("GeolocationBundle:MapsVille");
                    $found_cities = $repo_city->getCityByNameOrPostalCode($city_term);

                    if($found_cities == null && $city_id == "")
                    {
                        array_push($error_cities, "La ville que vous avez saisie n'est pas répertoriée dans notre base de données. Saisissez une ville proche de la votre s'il vous plait !");
                    }

                    //Par défaut la ville est null
                    $city = null;

                    //Si on dans les villes trouvés il n y en a qu'une alors on prend celle-ci
                    //Si le city_id n'est vide alors on a choisi une ville dans la liste.
                    //alors on la cherche dans la BDD au cas où et on prend celle-ci
                    if(count($found_cities) == 1)
                    {
                        $city = $found_cities[0];
                    }
                    else if($city_id != "")
                    {
                        $city = $repo_city->findOneBy(array('id' => $city_id));
                    }

                    if($form->isValid() && count($error_cities) == 0 && $city != null)
                    {
                        $userProfile->setCity($city);

                        //Si l'utilisateur n'à pas complété son profil alors on persist et on met à true
                        // le profil a bien été complété
                        if($connectedUser->getProfileIsCompleted() == false)
                        {
                            $em->persist($userProfile);
                            $connectedUser->setProfileIsCompleted(true);
                        }

                        $userProfile->setUpdatedAt(new \DateTime());

                        $connectedUser->setUserProfile($userProfile);

                        $em->flush();

                        //Redirection vers la page d'accueil
                        return $this->redirect($this->generateUrl('homepage'));
                    }
                }
            }
        }

        return $this->render('UserBundle:UserProfile:complete.html.twig',
                             array('form' => $form->createView(), 'cities' => $found_cities, "error_cities" => $error_cities,
                                    'city_term' => $city_term));
    }


    public function showUserAction($username)
    {
        $em = $this->getDoctrine()->getManager();
        //On récupère le destinataire en fonction du nom d'utilisateur
        $user = $em->getRepository("UserBundle:User")
                            ->findOneBy(array('username' => $username));

        if($user != null)
        {
            return $this->render('UserBundle:UserProfile:show.html.twig',
                                array("user" => $user));
        }
        else {
            return $this->render('UserBundle:UserProfile:show.html.twig',
                                array("error" => "L'utilisateur " . $username . " n'existe pas !"));
        }
    }
}
