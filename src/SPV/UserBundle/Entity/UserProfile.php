<?php

namespace SPV\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * UserProfile
 *
 * @ORM\Table(name="user_profile")
 * @ORM\Entity(repositoryClass="SPV\UserBundle\Repository\UserProfileRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class UserProfile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="age", type="integer")
     * @Assert\Type(
     *    type="integer",
     *    message="L'âge {{ value }} ne représente pas un entier !"
     * )
     * @Assert\NotBlank(message="Ce champ doit être saisi !")
     */
    private $age;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=10)
     * @Assert\NotBlank(message="Vous devez choisir votre sexe !")
     * @Assert\Choice(choices = {"m", "f"}, message = "Sélectionnez un sexe valide.")
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar_ext", type="string", length=255, nullable=true)
     */
    private $avatarExt;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="oneshot_description", type="string", length=255)
     * @Assert\NotBlank(message="Vous devez choisir une phrase parmi les propositions !")
     * @Assert\Choice(choices = {"1", "2", "3"}, message = "Sélectionnez une phrase valide.")
     */
    private $oneshotDescription;

    /**
    * @ORM\ManyToOne(targetEntity="SPV\GeolocationBundle\Entity\MapsVille", cascade={"persist"})
    * @ORM\JoinColumn(nullable=false, name="city_id", referencedColumnName="id_ville")
    */
    private $city;

    /**
    * @var datetime
    *
    * @ORM\Column(name="updatedAt", type="datetime")
    */
    protected $updatedAt;

    /*
    * @Assert\Image(maxSize="512k",
    *              maxSizeMessage="L'image ne doit pas déppaser les {{ maxSize }}ko.",
    *              minWidth = 64, minWidthMessage="La largeur de l'image doit faire {{ minWidth }}px au minimum.",
    *              minHeigth = 64, minHeigthMessage="La hauteur de l'image doit faire {{ minHeigth }}px au minimum.",
    *              maxWidth = 2056, maxWidthMessage="La largeur de l'image doit faire {{ maxWidth }}px au maximum.",
    *              maxHeigth = 2056, maxHeigthMessage="La hauteur de l'image doit faire {{ maxHeigth }}px au maximum."
    *
    * )
    */
    private $file;

    //Attribut pour stocker temporairement le nom du fichier
    private $tempFilename;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set age
     *
     * @param integer $age
     *
     * @return UserProfile
     */
    public function setAge($age)
    {
        $this->age = abs($age);

        return $this;
    }

    /**
     * Get age
     *
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return UserProfile
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set avatarExt
     *
     * @param string $avatarExt
     *
     * @return UserProfile
     */
    public function setAvatarExt($avatarExt)
    {
        $this->avatarExt = $avatarExt;

        return $this;
    }

    /**
     * Get avatarExt
     *
     * @return string
     */
    public function getAvatarExt()
    {
        return $this->avatarExt;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return UserProfile
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set oneshotDescription
     *
     * @param string $oneshotDescription
     *
     * @return UserProfile
     */
    public function setOneshotDescription($oneshotDescription)
    {
        $this->oneshotDescription = $oneshotDescription;

        return $this;
    }

    /**
     * Get oneshotDescription
     *
     * @return string
     */
    public function getOneshotDescription()
    {
        return $this->oneshotDescription;
    }

    /**
     * Set city
     *
     * @param \SPV\GeolocationBundle\Entity\MapsVille $city
     *
     * @return UserProfile
     */
    public function setCity(\SPV\GeolocationBundle\Entity\MapsVille $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \SPV\GeolocationBundle\Entity\MapsVille
     */
    public function getCity()
    {
        return $this->city;
    }

    public function getDescriptionSentence()
    {
        switch ($this->oneshotDescription) {
          case '1':
            return "Je désire fêter mon célibat comme il se doit !";
            break;

          case '2':
            return "Je désire célébrer mon célibat sobrement.";
            break;

          case '3':
            return "Je désire être surpris(e) ...";
            break;

          default:
            return "";
            break;
        }
    }

    //==========================================================================
    // Evènements pour doctrine et l'upload des images
    //==========================================================================
          public function setFile(UploadedFile $file)
          {
              $this->file = $file;
              $this->updatedAt = new \DateTime();

              //On vérifie si on avait déjà un fichier
              if($this->avatarExt != null)
              {
                  $this->tempFilename = $this->avatarExt;

                  //On réinitialise la photo
                  $this->avatarExt = null;
              }
          }

          public function getFile()
          {
              return $this->file;
          }

          public function getUploadDir() {
              //On retourne le chemin relatif
              return 'img/uploads/avatars';
          }

          protected function getUploadRootDir() {
              // the absolute directory path where uploaded documents should be saved
              return __DIR__ . '/../../../../web/' . $this->getUploadDir();
          }

          /**
           * @ORM\PrePersist()
           * @ORM\PreUpdate()
           */
          public function preUploadImage() {
              // the file property can be empty if the field is not required
              if (null === $this->file) {
                  return;
              }

              //Le nom du fichier est son id, on doit juste stocker son Extension
              //On stocke l'extension du fichier en BDD, d'où avatarExt (ext = extension)
              $this->avatarExt = $this->file->guessExtension();
          }

          /**
           * @ORM\PostPersist()
           * @ORM\PostUpdate()
           */
          public function upload()
          {
              //Si jamais il n'y a pas de photo champ facultatif
              if (null === $this->file) {
                  return;
              }

              //Si on avait un ancien fichier on le supprime
              if($this->tempFilename != null)
              {
                  $oldFile = $this->getUploadRootDir() . '/' . $this->id . '.' . $this->tempFilename;
                  if(file_exists($oldFile))
                  {
                      unlink($oldFile);
                  }
              }

              //On déplace le fichier envoyé dans le répertoire de notre choix
              $this->file->move(
                $this->getUploadRootDir(),
                $this->id . '.' . $this->avatarExt // le nom du fichier créé id + extension
              );
          }

          /**
           * @ORM\PreRemove()
           */
          public function preRemoveUpload()
          {
              //On sauvegarde temporairement le nom du fichier
              $this->tempFilename = $this->getUploadRootDir() . '/' . $this->id . '.' . $this->avatarExt;
          }

          /**
           * @ORM\PostRemove()
           */
          public function removeUpload()
          {
              //En post remove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
              if(file_exists($this->tempFilename))
              {
                  //On supprime le fichier
                  unlink($this->tempFilename);
              }
          }

          public function getWebPath()
          {
              return $this->getUploadDir() . '/' . $this->getId() . '.' . $this->getAvatarExt();
          }

          public function getRelativeWebPath()
          {
              return $this->getUploadRootDir() . '/' . $this->getId() . '.' . $this->getAvatarExt();
          }
    //==========================================================================

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return UserProfile
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    //==========================================================================
    // Connaître l'orientation de l'image et déterminé dans quel sens l'afficher
    //==========================================================================
        function getImageRotation() {

            $filename = $this->getRelativeWebPath();

            $exif = null;

            if(function_exists('exif_read_data')) {
                if($this->avatarExt == "jpg" || $this->avatarExt == "jpeg" || $this->avatarExt == "tiff")
                {
                    $exif = exif_read_data($filename);
                }
            }

            $orientation = isset($exif['Orientation']) ? $exif['Orientation'] : null;

            if (!empty($orientation) && !empty($exif) && $exif !== false) {
                switch ($orientation) {
                    case 3:
                        $orientation = 180;
                        break;

                    case 6:
                        $orientation = 90;
                        break;

                    case 8:
                        $orientation = 270;
                        break;
                }
            }

            return $orientation;
        }
    //==========================================================================
}
