<?php
// src/AppBundle/Entity/User.php

namespace SPV\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="SPV\UserBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var datetime
    *
    * @ORM\Column(name="register_date", type="datetime")
    */
    protected $register_date;

    /**
    * @var boolean
    *
    * @ORM\Column(name="hasAcceptedCGU", type="boolean")
    */
    private $hasAcceptedCGU;

    /**
    * @var boolean
    *
    * @ORM\Column(name="wantTips", type="boolean")
    */
    private $wantTips;

    /**
    * @ORM\OneToOne(targetEntity="SPV\UserBundle\Entity\UserProfile", cascade={"persist", "remove"})
    */
    private $user_profile;

    /**
    * @var boolean
    *
    * @ORM\Column(name="profileIsCompleted", type="boolean")
    */
    private $profileIsCompleted;

    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->register_date = new \DateTime;
        $this->hasAcceptedCGU = false;
        $this->wantTips = false;
        $this->profileIsCompleted = false;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return User
     */
    public function setRegisterDate($registerDate)
    {
        $this->register_date = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->register_date;
    }

    /**
     * Set hasAcceptedCGU
     *
     * @param boolean $hasAcceptedCGU
     *
     * @return User
     */
    public function setHasAcceptedCGU($hasAcceptedCGU)
    {
        $this->hasAcceptedCGU = $hasAcceptedCGU;

        return $this;
    }

    /**
     * Get hasAcceptedCGU
     *
     * @return boolean
     */
    public function getHasAcceptedCGU()
    {
        return $this->hasAcceptedCGU;
    }

    /**
     * Set wantTips
     *
     * @param boolean $wantTips
     *
     * @return User
     */
    public function setWantTips($wantTips)
    {
        $this->wantTips = $wantTips;

        return $this;
    }

    /**
     * Get wantTips
     *
     * @return boolean
     */
    public function getWantTips()
    {
        return $this->wantTips;
    }

    /**
     * Set userProfile
     *
     * @param \SPV\UserBundle\Entity\UserProfile $userProfile
     *
     * @return User
     */
    public function setUserProfile(\SPV\UserBundle\Entity\UserProfile $userProfile = null)
    {
        $this->user_profile = $userProfile;

        return $this;
    }

    /**
     * Get userProfile
     *
     * @return \SPV\UserBundle\Entity\UserProfile
     */
    public function getUserProfile()
    {
        return $this->user_profile;
    }

    /**
     * Set profileIsCompleted
     *
     * @param boolean $profileIsCompleted
     *
     * @return User
     */
    public function setProfileIsCompleted($profileIsCompleted)
    {
        $this->profileIsCompleted = $profileIsCompleted;

        return $this;
    }

    /**
     * Get profileIsCompleted
     *
     * @return boolean
     */
    public function getProfileIsCompleted()
    {
        return $this->profileIsCompleted;
    }
}
