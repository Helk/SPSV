<?php

namespace SPV\UserBundle\Repository;

class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function getQBUsersInCity($idCity, $user)
    {
        $qb = $this->createQueryBuilder('user');
        $qb->select('u', 'up')
        ->from('UserBundle:User', 'u')
        ->leftJoin('u.user_profile', 'up')
        ->where('up.city = :cid')
        ->setParameter('cid', $idCity)
        ->andWhere('u.id != :uid')
        ->setParameter('uid', $user);

        return $qb;
    }

    public function getQBUsersInCities($cities, $user)
    {
        $qb = $this->createQueryBuilder('user');
        $qb->select('u', 'up')
        ->from('UserBundle:User', 'u')
        ->leftJoin('u.user_profile', 'up')
        ->where('up.city in (:cities)')
        ->setParameter('cities', $cities)
        ->andWhere('u.id != :uid')
        ->setParameter('uid', $user);

        return $qb;
    }

    public function getQBUsersWithAge($qb, $age)
    {
        $qb->andWhere('up.age = :age')
            ->setParameter('age', $age);
            ;

        return $qb;
    }

    public function getQBUsersWithGenders($qb, $genders)
    {
        return $this->getQBWithArray($qb, $genders, "up", "gender");
    }

    public function getQBUsersWithDescriptionSentences($qb, $sentences)
    {
        return $this->getQBWithArray($qb, $sentences, 'up', 'oneshotDescription');
    }

    private function getQBWithArray($qb, $array, $table_name, $param_name)
    {
        $sql = "";
        $i = 0;

        foreach ($array as $item) {
          $sql .= $table_name . "." . $param_name . " LIKE :" . $param_name . $i ;

          if(end($array) != $item)
          {
              $sql .= " OR ";
          }

          $qb->setParameter($param_name . $i, $item);
          $i++;
        }

        $qb->andWhere($sql);
        return $qb;
    }
}
