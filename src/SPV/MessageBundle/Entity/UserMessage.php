<?php

namespace SPV\MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserMessage
 *
 * @ORM\Table(name="user_message")
 * @ORM\Entity(repositoryClass="SPV\MessageBundle\Repository\UserMessageRepository")
 */
class UserMessage
{
    /**
     * @ORM\ManyToOne(targetEntity="SPV\MessageBundle\Entity\Conversation", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @ORM\Id
     */
    private $conversation;

    /**
     * @ORM\ManyToOne(targetEntity="SPV\MessageBundle\Entity\Message", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @ORM\Id
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="SPV\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @ORM\Id
     */
    private $user;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isRead", type="boolean")
     */
    private $isRead;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->isRead = false;
    }

    /**
     * Set isRead
     *
     * @param boolean $isRead
     *
     * @return UserMessage
     */
    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;

        return $this;
    }

    /**
     * Get isRead
     *
     * @return boolean
     */
    public function getIsRead()
    {
        return $this->isRead;
    }

    /**
     * Set conversation
     *
     * @param \SPV\MessageBundle\Entity\Conversation $conversation
     *
     * @return UserMessage
     */
    public function setConversation(\SPV\MessageBundle\Entity\Conversation $conversation)
    {
        $this->conversation = $conversation;

        return $this;
    }

    /**
     * Get conversation
     *
     * @return \SPV\MessageBundle\Entity\Conversation
     */
    public function getConversation()
    {
        return $this->conversation;
    }

    /**
     * Set message
     *
     * @param \SPV\MessageBundle\Entity\Message $message
     *
     * @return UserMessage
     */
    public function setMessage(\SPV\MessageBundle\Entity\Message $message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return \SPV\MessageBundle\Entity\Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set user
     *
     * @param \SPV\UserBundle\Entity\User $user
     *
     * @return UserMessage
     */
    public function setUser(\SPV\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \SPV\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
