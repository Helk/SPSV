<?php

namespace SPV\MessageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use SPV\MessageBundle\Form\MessageType;
use SPV\MessageBundle\Entity\Message;
use SPV\MessageBundle\Entity\UserMessage;
use SPV\MessageBundle\Entity\Conversation;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $connectedUser = $this->getUser();

        if($connectedUser != null)
        {
            $repo_message = $this->getDoctrine()->getManager()
                               ->getRepository("MessageBundle:UserMessage");

            $user_messages = $repo_message->getUserMessagesConversationGroup($connectedUser->getId());

            $unread_messages = $repo_message->getUnreadMessages($connectedUser->getId());

            return $this->render('MessageBundle::index.html.twig',
                                  array('user_messages' => $user_messages, 'unread_messages' => $unread_messages));
        }

        return $this->render('MessageBundle::index.html.twig');
    }

    public function sendAction(Request $request, $user_id)
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $connectedUser = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $repo_user = $em->getRepository("UserBundle:User");
        $user_recipient = $repo_user->find($user_id);

        $view = array();

        if($connectedUser != null && $user_recipient != null)
        {
            $view["user_name"] = $user_recipient->getUsername();
            $view["user_id"] = $user_recipient->getId();

            if($request->getMethod() == 'POST')
            {
                $form->handleRequest($request);

                if($form->isValid())
                {
                    $message->setCreatedBy($connectedUser);
                    $message->setCreatedAt(new \DateTime);

                    $um = new UserMessage();
                    $um->setMessage($message);
                    $um->setUser($user_recipient);
                    $um->setConversation(new Conversation());

                    $em->persist($um);
                    $em->flush();

                    //Envoie de l'email de notification à l'utilisateur
                    $this->sendNotification($connectedUser, $user_recipient);

                    $this->get('session')->getFlashBag()->add('success', "Le message a été envoyé ");

                    return $this->redirect($this->generateUrl('message_homepage'));
                }
            }
        }
        else {
            $view["error"] = "Cette utilisateur n'existe pas !";
        }

        $view["form"] = $form->createView();

        return $this->render('MessageBundle::send.html.twig', $view);
    }

    public function respondAction(Request $request, $conv_id)
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $connectedUser = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $repo_message = $em->getRepository("MessageBundle:UserMessage");
        $conversation = $em->getRepository("MessageBundle:Conversation")->find($conv_id);

        $view = array();

        //Vérification si la conversation est valide
        $isOk = $this->checkConversation($conv_id);

        if($isOk)
        {
            $view["conv_id"] = $conversation->getId();

            if($request->getMethod() == 'POST')
            {
                $form->handleRequest($request);

                $umsg = $repo_message->getConversation($conv_id);

                //Si le sujet est null ou vide alors on prend le dernier sujet
                if($message->getSubject() == null || $message->getSubject() == "")
                {
                    $message->setSubject(end($umsg)->getMessage()->getSubject());
                }

                if($form->isValid() || $message->getMessage() != null)
                {
                    $message->setCreatedBy($connectedUser);
                    $message->setCreatedAt(new \DateTime);

                    $umes_recipients = $repo_message->getRecipients($conv_id, $connectedUser->getId());

                    $nbRecipient = count($umes_recipients);

                    if($nbRecipient == 0)
                    {
                        $umes_recipients = array($umsg[0]->getMessage()->getCreatedBy());
                    }

                    //Pour chaque destinataires on ajout le message
                    foreach ($umes_recipients as $umes_r) {
                        //Si le nombre de destinataire équivaut à zéro ça veut Directory
                        //qu'on n'a pas envoyé de message à l'autre utilisateur
                        //donc on envoie le message à l'envoyeur
                        if($nbRecipient != 0)
                        {
                          $user_recipient = $umes_r->getUser();
                        } else {
                          $user_recipient = $umes_r;
                        }
                        $um = new UserMessage();
                        $um->setMessage($message);
                        $um->setUser($user_recipient);
                        $um->setConversation($conversation);

                        $em->persist($um);
                        $em->flush();

                        //Envoie de l'email de notification à l'utilisateur
                        $this->sendNotification($connectedUser, $user_recipient);
                    }

                    $this->get('session')->getFlashBag()->add('success', "Le message a été envoyé ");

                    return $this->redirect($this->generateUrl('message_homepage'));
                }
            }
        }
        else {
            $view["error"] = "Cette conversation n'existe pas !";
        }

        $view["form"] = $form->createView();

        return $this->render('MessageBundle::temp_write_message.html.twig', $view);
    }

    public function showAction(Request $request, $conv_id)
    {
        $em = $this->getDoctrine()->getManager();
        $repo_message = $em->getRepository("MessageBundle:UserMessage");

        $isOk = $this->checkConversation($conv_id);
        $view = array();

        if($isOk)
        {
            $umes = $repo_message->getConversation($conv_id);

            foreach ($umes as $user_message) {
              if($user_message->getIsRead() == false
                 && $user_message->getUser()->getId() == $this->getUser()->getId())
              {
                  $user_message->setIsRead(true);
              }
            }

            $view["conversation"] = $umes;
            $view["conv_id"] = $umes[0]->getConversation()->getId();
            $em->flush();

            return $this->render('MessageBundle::show.html.twig', $view);
        }
        else {
            $view["error"] = "Cette conversation n'existe pas !";
        }

        return $this->render('MessageBundle::show.html.twig', $view);
    }

    private function checkConversation($conv_id)
    {
        $connectedUser = $this->getUser();

        if($connectedUser == null)
        {
            return false;
        }

        $conversation = $this->getDoctrine()->getManager()->getRepository("MessageBundle:Conversation")->find($conv_id);

        if($conversation == null)
        {
            return false;
        }

        $umes = $this->getDoctrine()->getManager()
                   ->getRepository("MessageBundle:UserMessage")
                   ->getConversationWithUser($conv_id, $connectedUser);

        if($umes == null)
        {
            return false;
        }

        return true;
    }

    private function sendNotification($from, $to)
    {
      //Envoi du mail d'activation
      $this->sendMail('Message de ' . $from->getUsername(),
                                     'Email/Message/sendNotification.html.twig',
                                     $to->getEmail(),
                                     array('name' => $from->getUsername(),
                                           'link' => $this->generateUrl('message_homepage', array(), UrlGeneratorInterface::ABSOLUTE_URL)));
    }

    private function sendMail($subject, $view, $email, $params)
    {
        //Récupération du service de mail
        $mailer = $this->get('mailer');

        $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom('nosupply@seulpourlasaintvalentin.fr')
                    ->setTo($email)
                    ->setBody($this->renderView($view, $params), 'text/html');

        //Envoie de l'email
        $mailer->send($message);
    }
}
