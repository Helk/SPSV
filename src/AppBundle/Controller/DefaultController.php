<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AppBundle::index.html.twig');
    }

    public function cguAction()
    {
        return $this->render('AppBundle::cgu.html.twig');
    }

    public function homeSecondAction()
    {
        return $this->render('AppBundle::home_s.html.twig');
    }
}
