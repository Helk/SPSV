<?php

namespace SearchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use SearchBundle\Form\FilterType;
use SearchBundle\Entity\Filter;

class DefaultController extends Controller
{
    public function searchResultAction(Request $request)
    {
        $filter = new Filter();
        $form = $this->createForm(FilterType::class, $filter);

        //===========================================
        // Paramètres de la ville
        //===========================================
          $found_cities = array();
          $error_cities = array();

          $city_id = $request->request->get('cities');
          $city_term =  $request->request->get('city_term');
        //===========================================

        $userResults = array();

        if($request->getMethod() == 'POST')
        {
            //On fait le lien entre la requête et le formulaire Request <=> Form
            $form->handleRequest($request);

            $em = $this->getDoctrine()->getManager();
            $repo_city = $em->getRepository("GeolocationBundle:MapsVille");
            $found_cities = $repo_city->getCityByNameOrPostalCode($city_term);

            $error_cities = $this->verifCity($city_term, $city_id, $found_cities);
            $city = $this->getCity($repo_city, $found_cities, $city_id);

            if(count($error_cities) == 0 && $city != null)
            {
                //On met le vrai nom de la ville trouvée
                $city_term = $city->getNom();

                $repo_user = $em->getRepository("UserBundle:User");

                //Récupération des utilisateurs en fonction de la ville et du filtre
                $userResults = $this->getUsers($city, $filter, $repo_user, $repo_city);

                //Récupération des soirées
                $repo_party = $em->getRepository("PartyBundle:Party");
                $parties = $this->getParties($city, $filter, $repo_party, $repo_city);

                //RÉINITIALISATION DU FORMULAIRE DE Recherche
                $form = $this->createForm(FilterType::class, new Filter());

                return $this->render('SearchBundle::searchResult.html.twig',
                                      array('form' => $form->createView(), "cities" => $found_cities, "error_cities" => $error_cities,
                                            'userResults' => $userResults,
                                            'parties' => $parties,
                                            'city_term' => $city_term));
            }

        }

        return $this->render('SearchBundle::searchResult.html.twig',
                              array('form' => $form->createView(), "cities" => $found_cities, "error_cities" => $error_cities));
    }

    private function getUsers($city, $filter, $repo_user, $repo_city)
    {
        $qb;
        $user = $this->getUser();

        if($filter->hasKmRay())
        {
            $cities = $repo_city->getCitiesByCityAndDistance($city, $filter->getKmRay());
            $qb = $repo_user->getQBUsersInCities($cities, $user);
        }
        else
        {
            $qb = $repo_user->getQBUsersInCity($city->getId(), $user);
        }

        if($filter->hasGender())
        {
            $qb = $repo_user->getQBUsersWithGenders($qb, $filter->getGender());
        }

        if($filter->hasAge())
        {
            $qb = $repo_user->getQBUsersWithAge($qb, $filter->getAge());
        }

        if($filter->hasOneShotDescription())
        {
            $qb = $repo_user->getQBUsersWithDescriptionSentences($qb, $filter->getOneshotDescription());
        }

        //On retourne les résultats de la requête !!
        return $qb->getQuery()->getResult();
    }

    private function getParties($city, $filter, $repo_party, $repo_city)
    {
        $parties;

        if($filter->hasKmRay())
        {
            $cities = array();
            $regions = array();
            $cp = array();

            $allCities = $repo_city->getCitiesByCityAndDistance($city, $filter->getKmRay());

            foreach ($allCities as $city) {
                array_push($cities, $city->getNom());
                array_push($regions, $city->getDepartement()->getRegion()->getNomRegion());
                array_push($cp, $city->getCp());
            }

            $parties = $repo_party->getAllPartiesByDistance($cities, $regions, $cp);
        }
        else
        {
            $parties = $repo_party->getAllParties($city->getNom(),
                            $city->getDepartement()->getRegion()->getNomRegion(),
                            $city->getCp());
        }

        //On retourne les résultats de la requête !!
        return $parties;
    }

    private function verifCity($city_term, $city_id, $found_cities)
    {
        $isCityTermOk = $city_term != "" || $city_term != null;
        $error_cities = array();

        if($isCityTermOk != true && $city_id == "")
        {
            array_push($error_cities, "Entrez une ville s'il vous plait !");
        }

        if($found_cities == null && $city_id == "")
        {
            array_push($error_cities, "La ville que vous avez saisie n'est pas répertoriée dans notre base de données. Saisissez une ville proche de la votre s'il vous plait !");
        }

        return $error_cities;
    }

    private function getCity($repo_city, $found_cities, $city_id)
    {
        //Par défaut la ville est null
        $city = null;

        //Si on dans les villes trouvés il n y en a qu'une alors on prend celle-ci
        //Si le city_id n'est vide alors on a choisi une ville dans la liste.
        //alors on la cherche dans la BDD au cas où et on prend celle-ci
        if(count($found_cities) == 1)
        {
            $city = $found_cities[0];
        }
        else if($city_id != "")
        {
            $city = $repo_city->findOneBy(array('id' => $city_id));
        }

        return $city;
    }

    public function searchCityArroundAction($city_id)
    {
        return $this->searchArround($city_id, true);
    }

    public function searchPartyArroundAction($city_id)
    {
        return $this->searchArround($city_id, false);
    }

    public function searchArround($city_id, $isUser)
    {
        $em = $this->getDoctrine()->getManager();
        $repo_city = $em->getRepository("GeolocationBundle:MapsVille");
        $city = $repo_city->find($city_id);

        if($city != null)
        {
            $userResults = array();
            $city_term =  $city->getNom();

            $km = 60;
            $filter = new Filter();
            $filter->setKmRay($km);

            if($isUser)
            {
                $repo_user = $em->getRepository("UserBundle:User");
                $userResults = $this->getUsers($city, $filter, $repo_user, $repo_city);

                return $this->render('SearchBundle::temp_users.html.twig',
                                      array('userResults' => $userResults,
                                            'city_term' => $city_term));
            }
            else
            {
              $repo_party = $em->getRepository("PartyBundle:Party");
              $parties = $this->getParties($city, $filter, $repo_party, $repo_city);

              return $this->render('PartyBundle::temp_party.html.twig',
                                    array('parties' => $parties,
                                          'city_term' => $city_term));
            }
        }

        if($user) {
            return $this->render('SearchBundle::temp_users.html.twig');
        }
        else {
            return $this->render('PartyBundle::temp_party.html.twig');
        }
    }
}
