<?php

namespace SearchBundle\Form\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class KilometerType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array(
                'Aucun' => '',
                '20 km' => '20',
                '30 km' => '30',
                '50 km' => '50',
                '70 km' => '70',
                '100 km' => '100',
            )
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
