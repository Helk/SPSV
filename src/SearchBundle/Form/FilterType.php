<?php
// src/AppBundle/Form/RegistrationType.php

namespace SearchBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use SPV\UserBundle\Form\FormType\DescriptionSentenceType;
use SPV\UserBundle\Form\FormType\GenderType;
use SearchBundle\Form\FormType\KilometerType;

class FilterType extends AbstractType
{
      public function buildForm(FormBuilderInterface $builder, array $options)
      {
          $builder->add('age', IntegerType::class, array('label' => 'Age', 'required' => false))
                  ->add('gender', GenderType::class, array('label' => 'Sexe',
                                                           'expanded' => true,
                                                           'multiple' => true,
                                                           'required' => false
                                                         ))
                  ->add('kmRay', KilometerType::class, array('label' => 'Recherche dans un rayon',
                                                             'expanded' => false,
                                                             'multiple' => false,
                                                             'required' => false
                                                           ))
                   ->add('oneshotDescription', DescriptionSentenceType::class, array('label' => 'Quelle phrase',
                                                            'expanded' => true,
                                                            'multiple' => true,
                                                            'required' => false
                                                          ))
          ;
      }

      public function configureOptions(OptionsResolver $resolver)
      {
          $resolver->setDefaults(array(

          ));
      }

      public function getBlockPrefix()
      {
          return 'user_profile_registration';
      }
}
