<?php

namespace SearchBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Filter
{

    /**
     * @Assert\Type(
     *    type="integer",
     *    message="L'âge {{ value }} ne représente pas un entier !"
     * )
     */
    private $age;

    private $gender;

    /**
     * @Assert\Choice(choices = {"", "20", "30", "50", "70", "100"}, message = "Sélectionnez un rayon valide.")
     */
    private $kmRay;

    private $oneshotDescription;

    public function __construct()
    {
        $this->age = null;
        $this->kmRay = null;
        $this->gender = null;
        $this->oneshotDescription = null;
    }


    public function setAge($age)
    {
        $this->age = abs($age);

        return $this;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setKmRay($kmRay)
    {
        $this->kmRay = $kmRay;

        return $this;
    }

    public function getKmRay()
    {
        return $this->kmRay;
    }

    /**
     * Set oneshotDescription
     *
     * @param string $oneshotDescription
     *
     * @return Filter
     */
    public function setOneshotDescription($oneshotDescription)
    {
        $this->oneshotDescription = $oneshotDescription;

        return $this;
    }

    /**
     * Get oneshotDescription
     *
     * @return string
     */
    public function getOneshotDescription()
    {
        return $this->oneshotDescription;
    }

    public function hasKmRay()
    {
        return $this->kmRay != null || $this->kmRay != "";
    }

    public function hasAge()
    {
        return ($this->age != null || $this->age != "") && is_integer($this->age);
    }

    public function hasOneShotDescription()
    {
        $has = false;

        if(is_array($this->oneshotDescription))
        {
            $has = count($this->oneshotDescription) > 0;
        }
        else {
          $has = $this->oneshotDescription != null || $this->oneshotDescription != "";
        }

        return $has;
    }

    public function hasGender()
    {
        $has = false;

        if(is_array($this->gender))
        {
            $has = count($this->gender) > 0;
        }
        else {
          $has = $this->gender != null || $this->gender != "";
        }

        return $has;
    }

}
